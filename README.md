Keys
===

Feature
---

### Secure key storage
We use AES algorithm to encrypt your keys.

### Secure copy
We use content provider to make sure the key you copied can be pasted once.

Update logs
---

#### V0.6.5.7057
* Enhance permission requesting callback

#### V0.6.5.7000
* Permission requesting on Android M.

#### V0.6.5.6992
* Fix bug: the master key can be changed to invalid length.

#### V0.6.5.6987
* Change icon
* White text color in recent view

#### v0.6.5.6977
* Secure copy was added

#### v0.6.5.6887
* KeyDetailView built

### Developing Version

#### v0.6.5.6882
* Complete Backup, Restore, ChangeMasterKey functions

#### v0.1.3.2343
* Restore function

#### v0.1.3.2313
* Backup function

#### v0.1.3.2264
* Rename app from RememberPassword to Keys.

#### v0.1.3.2260
* SettingView built
* Both English and Chinese are supported.

#### v0.1.3.2224
* First version