package com.macaroni.androidLib;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;

public class TransitionLayout extends FrameLayout {
	private final TransitionType DEFAULT_TRANSITION_TYPE = TransitionType.Radial;
	private final int DEFAULT_DUR = 500;
	private final TimeInterpolator DEFAULT_INTERPOLATOR = new AccelerateInterpolator();

	private ValueAnimator.AnimatorUpdateListener animationEndListener;
	private Path clipPath;
	private int curIndex;
	private int previousIndex;
	private int x,y;
	private int clearColor = 0xFFFFFFFF;
	private float radius;
	private TransitionType curTransitionType;
	private boolean isAnimating = false;
	private boolean isInAnimation;
	private boolean isTemporaryIgnoreTouch = false;

	private int overlayViewIndex = -1;

	private int topmostViewIndex = -1;

	public enum TransitionType {
		Radial, NONE
	}

	public TransitionLayout(Context context) {
		this(context, null);
	}

	public TransitionLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public TransitionLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	private void init() {
		animationEndListener = new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				if(animation.getAnimatedFraction() == 1.0f) {
					isTemporaryIgnoreTouch = false;
					isAnimating = false;
					curTransitionType = TransitionType.NONE;
				}
			}
		};
		clipPath = new Path();
		setClipChildren(false);
	}

	public void setHotspot(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setHotspot(View view) {
		int[] location = new int[2];
		view.getLocationOnScreen(location);
		int[] location2 = new int[2];
		getLocationOnScreen(location2);
		this.x = location[0] - location2[0] + view.getWidth() / 2;
		this.y = location[1] - location2[1] + view.getHeight() / 2;
	}

	public void startTransition(int toChild) {
		startTransition(toChild, DEFAULT_TRANSITION_TYPE);
	}

	public void startTransition(int toChild, TransitionType transitionType) {
		startTransition(toChild, transitionType, DEFAULT_DUR);
	}

	public void startTransition(int toChild, TransitionType transitionType, int duration) {
		startTransition(toChild, transitionType, duration, true);
	}

	public void startTransition(int toChild, TransitionType transitionType, int duration, boolean in) {
		startTransition(toChild, transitionType, duration, in, DEFAULT_INTERPOLATOR);
	}

	public void startTransition(int toChild, TransitionType transitionType, int duration, boolean in, TimeInterpolator interpolator) {
		if(isAnimating)
			return;
		isAnimating = true;
		previousIndex = curIndex;
		curIndex = toChild;
		isInAnimation = in;
		curTransitionType = transitionType;
		switch(transitionType) {
			case Radial:
				startRadialTransition(duration, interpolator);
				break;
		}
	}

	private void startRadialTransition(int duration, TimeInterpolator interpolator) {
		float dist = (float) Math.sqrt(x * x + y * y);
		dist = (float) Math.max(dist, Math.sqrt((getWidth() - x) * (getWidth() - x) + y * y));
		dist = (float) Math.max(dist, Math.sqrt(x * x + (getHeight() - y) * (getHeight() - y)));
		dist = (float) Math.max(dist, Math.sqrt((getWidth() - x) * (getWidth() - x) + (getHeight() - y) * (getHeight() - y)));

		ValueAnimator animator = ValueAnimator.ofFloat(isInAnimation ? 0 : dist, isInAnimation ? dist : 0);
		animator.setDuration(duration);
		animator.setInterpolator(interpolator);
		animator.addUpdateListener(animationEndListener);
		animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				radius = (Float) animation.getAnimatedValue();
				postInvalidate();
			}
		});
		animator.start();
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		canvas.drawColor(clearColor);
		int bottomViewIndex = isInAnimation ? previousIndex : curIndex;
		int topViewIndex = isInAnimation ? curIndex : previousIndex;
		if(curTransitionType == TransitionType.Radial) {
			clipPath.reset();
			clipPath.addCircle(x, y, Math.max(1, radius), Path.Direction.CW);
			if(topmostViewIndex == bottomViewIndex) {
				drawChild(canvas, getChildAt(bottomViewIndex), getDrawingTime());
				canvas.save();
				canvas.clipPath(clipPath);
				drawChild(canvas, getChildAt(topViewIndex), getDrawingTime());
				if(overlayViewIndex != -1)
					drawChild(canvas, getChildAt(overlayViewIndex), getDrawingTime());
				canvas.restore();
			/*}else if(topmostViewIndex == topViewIndex) {
				drawChild(canvas, getChildAt(topViewIndex), getDrawingTime());
				canvas.save();
				canvas.clipPath(clipPath);
				drawChild(canvas, getChildAt(bottomViewIndex), getDrawingTime());
				if(overlayViewIndex != -1)
					drawChild(canvas, getChildAt(overlayViewIndex), getDrawingTime());
				canvas.restore();*/
			}else{
				drawChild(canvas, getChildAt(bottomViewIndex), getDrawingTime());
				canvas.save();
				canvas.clipPath(clipPath);
				drawChild(canvas, getChildAt(topViewIndex), getDrawingTime());
				canvas.restore();
				if(overlayViewIndex != -1)
					drawChild(canvas, getChildAt(overlayViewIndex), getDrawingTime());
			}
		}else{
			if(curIndex == topmostViewIndex) {
				if(overlayViewIndex != -1)
					drawChild(canvas, getChildAt(overlayViewIndex), getDrawingTime());
				drawChild(canvas, getChildAt(curIndex), getDrawingTime());
			}else{
				drawChild(canvas, getChildAt(curIndex), getDrawingTime());
				if(overlayViewIndex != -1)
					drawChild(canvas, getChildAt(overlayViewIndex), getDrawingTime());
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return false;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		boolean flag = isTemporaryIgnoreTouch;
		if(!flag && curIndex == topmostViewIndex)
			flag = getChildAt(curIndex).dispatchTouchEvent(ev);
		if(!flag)
			flag = getChildAt(overlayViewIndex).dispatchTouchEvent(ev);
		if(!flag/* && !isAnimating*/)
			flag = getChildAt(getCurrentChild()).dispatchTouchEvent(ev);
		return flag;
	}

	public int getCurrentChild() {
		return curIndex;
	}

	public void setCurrentChild(int index) {
		curIndex = index;
		postInvalidate();
	}

	public void setCurrentChildWithoutPostInvalidate(int index) {
		curIndex = index;
	}

	public int getClearColor() {
		return clearColor;
	}

	public void setClearColor(int clearColor) {
		this.clearColor = clearColor;
	}

	public void ignoreTouchTemporary() {
		isTemporaryIgnoreTouch = true;
	}

	public void setOverlayViewIndex(int overlayViewIndex) {
		this.overlayViewIndex = overlayViewIndex;
	}

	public boolean isAnimating() {
		return isAnimating;
	}

	public void setTopmostViewIndex(int topmostViewIndex) {
		this.topmostViewIndex = topmostViewIndex;
	}
}