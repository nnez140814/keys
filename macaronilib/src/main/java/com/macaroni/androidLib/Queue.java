package com.macaroni.androidLib;

@SuppressWarnings("unchecked")
public class Queue<Item> {
	private Item[] q;
	private int n;
	private int first;
	private int last;

	public Queue() {
		q = (Item[]) new Object[2];
		n = 0;
		first = 0;
		last = 0;
	}

	public boolean isEmpty() {
		return n == 0;
	}

	public int size() {
		return n;
	}

	private void resize(int capacity) {
		Item[] temp = (Item[]) new Object[capacity];
		for (int i = 0; i < n; i++) {
			temp[i] = q[(first + i) % q.length];
		}
		q = temp;
		first = 0;
		last  = n;
	}

	public void enqueue(Item item) {
		if (n == q.length) resize(2*q.length);
		q[last++] = item;
		if (last == q.length) last = 0;
		n++;
	}

	public Item dequeue() {
		if (isEmpty()) return null;
		Item item = q[first];
		q[first] = null;
		n--;
		first++;
		if (first == q.length) first = 0;
		if (n > 0 && n == q.length/4) resize(q.length/2);
		return item;
	}

	public Item getNext() {
		if (isEmpty()) return null;
		return q[first];
	}

}
