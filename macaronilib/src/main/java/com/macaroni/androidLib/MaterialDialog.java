package com.macaroni.androidLib;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import carbon.widget.Button;

public class MaterialDialog {
    private Context context;
    private View layout;
    private View contentView;
    private View.OnClickListener positiveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hide();
            if(customPositiveListener != null)
                customPositiveListener.onClick(v);
        }
    };
    private View.OnClickListener customPositiveListener;

    public MaterialDialog(Context context) {
        this.context = context;
        this.layout = View.inflate(this.context, R.layout.dialog, null);
    }

    public MaterialDialog setContentView(int redid) {
        contentView = View.inflate(context, redid, null);
        return this;
    }

    public MaterialDialog setContentView(View view) {
        contentView = view;
        return this;
    }

    public MaterialDialog setContent(String msg) {
        TextView textView = new TextView(context);
        textView.setText(msg);
        contentView = textView;
        return this;
    }

    public MaterialDialog setTitle(String title) {
        ((TextView) layout.findViewById(R.id.dialog_title)).setText(title);
        return this;
    }

    public MaterialDialog show() {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            layoutParams.flags = WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS;
        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            layoutParams.flags = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        }else{
            layoutParams.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        }
        layoutParams.format = PixelFormat.RGBA_8888;
        layoutParams.windowAnimations = R.style.Macaroni_Dialog;

        LinearLayout content = (LinearLayout) layout.findViewById(R.id.dialog_content);
        content.addView(contentView);
        Button positiveBtn = (Button) layout.findViewById(R.id.dialog_positive);
        positiveBtn.setOnClickListener(positiveListener);

        manager.addView(layout, layoutParams);
        return this;
    }

    public MaterialDialog hide() {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        manager.removeView(layout);
        return this;
    }

    public MaterialDialog setPositiveBtn(String text, View.OnClickListener listener) {
        ((Button) layout.findViewById(R.id.dialog_positive)).setText(text.toUpperCase());
        customPositiveListener = listener;
        return this;
    }

}
