package com.macaroni.androidLib;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import carbon.drawable.EdgeEffect;

public class ScrollView extends android.widget.ScrollView {
	public static class OverScrollMode {
		static final int ALWAYS = 0;
		static final int IF_CONTENT_SCROLLS = 1;
		static final int NEVER = 2;
	}

	private int overScrollMode;
	private int touchSlop;
	private boolean drag;
	private float prevY = 0f;
	private long prevScroll = 0;

	private Context context;
	private EdgeEffect topEffect, bottomEffect;

	public ScrollView(Context context) {
		super(context);
		init(context, null, android.R.attr.scrollViewStyle);
	}

	public ScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, android.R.attr.scrollViewStyle);
	}

	public ScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public ScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		init(context, attrs, defStyleAttr);
	}

	private void init(Context ctx, AttributeSet attrs, int defStyle) {
		context = ctx;
		final ViewConfiguration configuration = ViewConfiguration.get(getContext());
		touchSlop = configuration.getScaledTouchSlop();

		TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.macaroniLib, defStyle, 0);
		try {
			setOverScrollMode(array.getInteger(R.styleable.macaroniLib_ml_overScrollMode, OverScrollMode.NEVER));
		}finally{
			array.recycle();
		}

		setClipToPadding(false);
	}

	@Override
	public void setOverScrollMode(int overScrollMode) {
		this.overScrollMode = overScrollMode;
		if(overScrollMode != OverScrollMode.NEVER) {
			if(topEffect == null)
				topEffect = new EdgeEffect(context);
			if(bottomEffect == null)
				bottomEffect = new EdgeEffect(context);
		}
		super.setOverScrollMode(OVER_SCROLL_ALWAYS);
	}

	@Override
	public int getOverScrollMode() {
		return overScrollMode;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		switch(event.getAction()) {
			case MotionEvent.ACTION_MOVE:
				float deltaY = prevY - event.getY();

				if(!drag && Math.abs(deltaY) > touchSlop) {
					final ViewParent parent = getParent();
					if(parent != null) {
						parent.requestDisallowInterceptTouchEvent(true);
					}
					drag = true;
					if(deltaY > 0) {
						deltaY -= touchSlop;
					}else{
						deltaY += touchSlop;
					}
				}
				if(drag) {
					final int oldY = computeVerticalScrollOffset();
					int range = computeVerticalScrollRange() - getHeight();
					boolean canOverScroll = overScrollMode == OverScrollMode.ALWAYS ||
							(overScrollMode == OverScrollMode.IF_CONTENT_SCROLLS && range > 0);

					if(canOverScroll) {
						float pulledToY = oldY + deltaY;
						if(pulledToY < 0) {
							topEffect.onPull(deltaY / getHeight(), event.getX() / getWidth());
							if(!bottomEffect.isFinished())
								bottomEffect.onRelease();
						}else if(pulledToY > range) {
							bottomEffect.onPull(deltaY / getHeight(), 1.f - event.getX() / getWidth());
							if(!topEffect.isFinished())
								topEffect.onRelease();
						}
						if(topEffect != null && (!topEffect.isFinished() || !bottomEffect.isFinished()))
							postInvalidate();
					}
				}
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				if(drag) {
					drag = false;

					if(topEffect != null) {
						topEffect.onRelease();
						bottomEffect.onRelease();
					}
				}
				break;
		}
		prevY = event.getY();

		return super.dispatchTouchEvent(event);
	}

	@Override
	protected void onScrollChanged(int x, int y, int prevX, int prevY) {
		super.onScrollChanged(x, y, prevX, prevY);
		if (drag || topEffect == null)
			return;
		int range = computeVerticalScrollRange() - getHeight();
		boolean canOverScroll = overScrollMode == OverScrollMode.ALWAYS ||
				(overScrollMode == OverScrollMode.IF_CONTENT_SCROLLS && range > 0);
		if (canOverScroll) {
			long t = System.currentTimeMillis();
			int dy = y - prevY;
			int vely = (int) (dy * 1000.0f / (t - prevScroll));
			if (computeVerticalScrollOffset() == 0 && dy < 0) {
				topEffect.onAbsorb(-vely);
			} else if (computeVerticalScrollOffset() == range && dy > 0) {
				bottomEffect.onAbsorb(vely);
			}
			prevScroll = t;
		}
	}

}
