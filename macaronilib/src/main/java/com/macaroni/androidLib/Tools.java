package com.macaroni.androidLib;

public class Tools {
	public static boolean isArrayEqual(byte[] b1, byte[] b2) {
		if(b1.length == b2.length) {
			for(int i = 0; i < b1.length; i++) {
				if(b1[i] != b2[i]) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
