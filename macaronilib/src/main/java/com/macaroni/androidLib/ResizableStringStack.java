package com.macaroni.androidLib;

public class ResizableStringStack {
	private String[] array;
	private int N = 0;

	public ResizableStringStack() {
		this(1);
	}

	public ResizableStringStack(int initSize) {
		array = new String[initSize];
	}

	private void resize(int size) {
		String[] tmp = new String[size];
		for(int i = 0; i < N; i++) {
			tmp[i] = array[i];
		}
		array = tmp;
	}

	public void push(String b) {
		if(N == array.length)
			resize(2 * array.length);
		array[N++] = b;
	}

	public String pop() {
		String b = array[--N];
		if(N > 0 && N == array.length / 4)
			resize(array.length / 2);
		return b;
	}

	public int size() {
		return N;
	}

	public void clear() {
		N = 0;
		array = new String[1];
	}

	public String[] toArray() {
		String[] re = new String[N];
		for(int i = 0; i < N; i++) {
			re[i] = array[i];
		}
		return re;
	}

}
