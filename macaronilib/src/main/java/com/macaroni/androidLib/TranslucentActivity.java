package com.macaroni.androidLib;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class TranslucentActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SystemBarUtil.setBarsTranslucent(getWindow(), 0x20000000);
	}

	protected void setHeadersPadding(int height, View... views) {
		for(int i = 0; i < views.length; i++) {
			views[i].setPadding(0, height, 0, 0);
		}
	}

	protected void setFootersPadding(int height, View... views) {
		for(int i = 0; i < views.length; i++) {
			views[i].setPadding(0, 0, 0, height);
		}
	}

}
