package com.macaroni.androidLib;

public class ResizableIntStack {
	private int[] array;
	private int N = 0;

	public ResizableIntStack() {
		this(1);
	}

	public ResizableIntStack(int initSize) {
		array = new int[initSize];
	}

	private void resize(int size) {
		int[] tmp = new int[size];
		for(int i = 0; i < N; i++) {
			tmp[i] = array[i];
		}
		array = tmp;
	}

	public void push(int b) {
		if(N == array.length)
			resize(2 * array.length);
		array[N++] = b;
	}

	public int pop() {
		int b = array[--N];
		if(N > 0 && N == array.length / 4)
			resize(array.length / 2);
		return b;
	}

	public int size() {
		return N;
	}

	public void clear() {
		N = 0;
		array = new int[1];
	}

	public int[] toArray() {
		int[] re = new int[N];
		for(int i = 0; i < N; i++) {
			re[i] = array[i];
		}
		return re;
	}

	public byte[] toByteArray() {
		byte[] re = new byte[N];
		for(int i = 0; i < N; i++) {
			re[i] = (byte) array[i];
		}
		return re;
	}

}
