package me.tranglenull.keys.common;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;

/**
 * Created by Trangle on 17-10-22.
 */

public class FingerprintUtil {
    public static boolean isFingerprintSupported(Activity activity) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            KeyguardManager keyguardManager = (KeyguardManager) activity.getSystemService(Context.KEYGUARD_SERVICE);
            FingerprintManager fingerprintManager = (FingerprintManager) activity.getSystemService(Context.FINGERPRINT_SERVICE);
            if(!keyguardManager.isKeyguardSecure()) {
                return false;
            }
            if(!PermissionUtil.isFingerprintPermitted(activity)) {
                return false;
            }
            //noinspection MissingPermission
            if(!fingerprintManager.isHardwareDetected()) {
                return false;
            }
            //noinspection MissingPermission
            if(!fingerprintManager.hasEnrolledFingerprints()) {
                return false;
            }
            return true;
        }
        return false;
    }
}
