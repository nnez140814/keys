package me.tranglenull.keys.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

public class PermissionUtil {
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 0;
    private static final int USE_FINGERPRINT_REQUEST_CODE = 1;

    public static boolean isStorageAccessible(Context context) {
        //noinspection SimplifiableIfStatement
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED/* && context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED*/;
        }else{
            return true;
        }
    }

    public static boolean requestStorageAccessPermission(Activity activity) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(!activity.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                return false;
            }
            activity.requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
        }
        return true;
    }

    public static boolean isFingerprintPermitted(Context context) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.checkSelfPermission(Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED;
        }else{
            return true;
        }
    }

    public static boolean requestFingerprintPermission(Activity activity) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(!activity.shouldShowRequestPermissionRationale(Manifest.permission.USE_FINGERPRINT)) {
                return false;
            }
            activity.requestPermissions(new String[] {Manifest.permission.USE_FINGERPRINT}, USE_FINGERPRINT_REQUEST_CODE);
        }
        return true;
    }

}
