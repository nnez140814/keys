package me.tranglenull.keys.common;

import android.content.Context;
import com.macaroni.androidLib.ResizableIntStack;
import com.macaroni.androidLib.SecureUtil;
import me.tranglenull.keys.App;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class FileUtil {
	final static int CUR_VERSION = 1;

	final static String STORAGE_VERSION = "storage_version";
	final static String KEY_VERIFIER = "key_verifier";
	final static String DATABASE = "database.json";

	final static String LOG_FOLDER = "logs/";

	static File fileDir;
	static File storageVersionFile;
	static File keyVerifierFile;
	static File databaseFile;

	static File logFolder;

	private static Key correctKey;

	public static void init(Context context) {
		fileDir = context.getFilesDir();
		storageVersionFile = new File(fileDir, STORAGE_VERSION);
		keyVerifierFile = new File(fileDir, KEY_VERIFIER);
		databaseFile = new File(fileDir, DATABASE);
		logFolder = new File(fileDir, LOG_FOLDER);
		if(!logFolder.exists())
			logFolder.mkdirs();
	}

	public static boolean isFirstLaunch() {
		try {
			FileInputStream inputStream = new FileInputStream(storageVersionFile);
			int storageVersion = inputStream.read();
			if(storageVersion < CUR_VERSION) {
				moveAllConflictFiles();
				return true;
			}
		}catch(FileNotFoundException e) {
			e.printStackTrace();
			return true;
		}catch(IOException e) {
			e.printStackTrace();
			return true;
		}
		return false;
	}

	private static void moveAllConflictFiles() {
		keyVerifierFile.delete();
		databaseFile.delete();
	}

	public static void clearFirstLaunchFlag() {
		try {
			writeFile(storageVersionFile, CUR_VERSION);
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

	public static String saveKey(byte[] bytes) {
		Key key = SecureUtil.getAESKey(bytes);
		correctKey = key;
		DataUtil.init();
		try {
			byte[] enc = SecureUtil.AESEncrypt(KEY_VERIFIER.getBytes(), key);
			writeFile(keyVerifierFile, enc);
		}catch(Exception e) {
			e.printStackTrace();
			correctKey = null;
			return e.getMessage();
		}
		clearFirstLaunchFlag();
		return null;
	}

	public static boolean verifyKey(byte[] bytes) {
		Key key = SecureUtil.getAESKey(bytes);
		try {
			if(!Arrays.equals(SecureUtil.AESDecrypt(readFile(keyVerifierFile), key), KEY_VERIFIER.getBytes())) {
				return false;
			}
		}catch(Exception ignore) {
			return false;
		}
		correctKey = key;
		return true;
	}

	public static void clearKey() {
		correctKey = null;
	}

	public static void writeFile(File file, int oneByte) throws IOException {
		FileOutputStream outputStream = new FileOutputStream(file);
		outputStream.write(oneByte);
	}

	public static void writeFile(File file, byte[] bytes) throws IOException {
		FileOutputStream outputStream = new FileOutputStream(file);
		outputStream.write(bytes);
	}

	public static void writeFile(File file, byte[] bytes, int start) throws IOException {
		FileOutputStream outputStream = new FileOutputStream(file);
		outputStream.write(bytes, start, bytes.length - start);
	}

	public static void writeString(File file, String str) throws IOException {
		writeFile(file, str.getBytes());
	}

	public static byte[] readFile(File file) throws IOException {
		FileInputStream inputStream = new FileInputStream(file);
		byte[] bytes = new byte[inputStream.available()];
		inputStream.read(bytes);
		return bytes;
	}

	public static String readString(File file) throws IOException {
		return new String(readFile(file));
	}

	public static void log(Throwable ex) {
		try {
			writeFile(new File(logFolder, Long.toString(System.currentTimeMillis())), buildCrashCause(ex).getBytes());
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

	private static String buildCrashCause(Throwable ex) {
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);

		ex.printStackTrace(printWriter);
		Throwable cause = ex.getCause();
		while(cause != null) {
			cause.printStackTrace(printWriter);
			cause = cause.getCause();
		}

		printWriter.close();
		return writer.toString();
	}

	public static byte[] AESEncrypt(byte[] raw) throws IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException {
		return SecureUtil.AESEncrypt(raw, correctKey);
	}

	public static byte[] AESDecrypt(byte[] enc) throws IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException {
		return SecureUtil.AESDecrypt(enc, correctKey);
	}

	public static boolean backup() {
		ResizableIntStack buffer = new ResizableIntStack();
		try {
			FileInputStream inputStream = new FileInputStream(storageVersionFile);
			int storageVersion = inputStream.read();
			buffer.push(storageVersion);
		}catch(IOException e) {
			e.printStackTrace();
			return false;
		}

		try {
			byte[] keyVerifier = readFile(keyVerifierFile);
			buffer.push(keyVerifier.length / Byte.MAX_VALUE);
			buffer.push(keyVerifier.length % Byte.MAX_VALUE);
			for(int i = 0; i < keyVerifier.length; i++) {
				buffer.push(keyVerifier[i]);
			}
		}catch(IOException e) {
			e.printStackTrace();
			return false;
		}

		try {
			byte[] database = readFile(databaseFile);
			for(int i = 0; i < database.length; i++) {
				buffer.push(database[i]);
			}
		}catch(IOException e) {
			e.printStackTrace();
			return false;
		}

		try {
			File backupDir = new File(App.getAppBackupDir());
			if(!backupDir.exists())
				backupDir.mkdirs();
			File backupFile = new File(App.getAppBackupDir(), App.formatTime());
			writeFile(backupFile, buffer.toByteArray());
		}catch(IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static String[] getAllBackupNames() {
		File backupDir = new File(App.getAppBackupDir());
		return backupDir.list(new FilenameFilter() {
			@Override
			public boolean accept(File file, String filename) {
				return file.isDirectory();
			}
		});
	}

	public static boolean restore(String backupFileName, byte[] key) {
		try {
			byte[] backup = readFile(new File(App.getAppBackupDir(), backupFileName));
			int verifierLen = backup[1] * Byte.MAX_VALUE + backup[2];
			byte[] verifier = new byte[verifierLen];
			for(int i = 0; i < verifierLen; i++) {
				verifier[i] = backup[i + 3];
			}
			String dec = new String(SecureUtil.AESDecrypt(verifier, SecureUtil.getAESKey(key)));
			if(dec.equals(KEY_VERIFIER)) {
				writeFile(storageVersionFile, backup[0]);
				writeFile(keyVerifierFile, verifier);
				writeFile(databaseFile, backup, 3 + verifierLen);
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
