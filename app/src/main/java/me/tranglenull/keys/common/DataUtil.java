package me.tranglenull.keys.common;

import org.json.JSONArray;
import org.json.JSONObject;

public class DataUtil {
	private static JSONArray database;

	public static boolean setup() {
		database = new JSONArray();
		flushDB();
		return true;
	}

	public static boolean init() {
		try {
			database = new JSONArray(new String(FileUtil.AESDecrypt(FileUtil.readFile(FileUtil.databaseFile))));
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static void flushDB() {
		try {
			FileUtil.writeFile(FileUtil.databaseFile, FileUtil.AESEncrypt(database.toString().getBytes()));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean addRecord(String description, String account, String password) {
		if(isRecordExists(description)) {
			return false;
		}
		try {
			JSONObject newRecord = new JSONObject();
			newRecord.put("Description", description);
			newRecord.put("Account", account);
			newRecord.put("Password", password);
			database.put(newRecord);
		}catch(Exception e) {
			e.printStackTrace();
		}
		flushDB();
		return true;
	}

	private static boolean isRecordExists(String description) {
		try {
			int len = database.length();
			for(int i = 0; i < len; i++) {
				if(database.getJSONObject(i).getString("Description").equals(description)) {
					return true;
				}
			}
		}catch(Exception e) {
			FileUtil.log(e);
		}
		return false;
	}

	public static JSONArray getDatabase() {
		return database;
	}
}
