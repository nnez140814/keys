package me.tranglenull.keys.view;

import android.os.Handler;
import me.tranglenull.keys.common.DataUtil;
import me.tranglenull.keys.common.FileUtil;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;

import java.util.Arrays;

public class KeyChangeView extends KeyView {

	private int currentStep = 1;

	public KeyChangeView(Viewport viewport) {
		super(viewport);
		getHeaderTitle().setText(R.string.str_settings_masterKey_change);
	}

	@Override
	public void onShow() {
		super.onShow();
		clearKey();
		getHeaderMessage().setText(R.string.str_settings_masterKey_change_one);
		currentStep = 1;
	}

	@Override
	protected void onKeySubmit() {
		switch(currentStep) {
			case 1:
				stepOne(pwdStack.toByteArray());
				break;
			case 2:
				stepTwo(pwdStack.toByteArray());
				break;
			case 3:
				stepThree(pwdStack.toByteArray());
				break;
			default:
				throw new IllegalStateException("Unknown step: " + currentStep);
		}
	}

	private void stepOne(byte[] key) {
		clearKey();
		if(FileUtil.verifyKey(key)) {
			currentStep = 2;
			getHeaderMessage().setText(R.string.str_settings_masterKey_change_two);
			showMsg(viewport.getResString(R.string.str_settings_masterKey_change_two));
		}else{
			showError(viewport.getResString(R.string.str_key_wrong));
		}
	}

	private byte[] firstKey;
	private void stepTwo(byte[] key) {
		if(key.length < 4 || key.length > 10) {
			showError(viewport.getResString(R.string.str_key_size_invalid));
			return;
		}
		firstKey = key;
		currentStep = 3;
		clearKey();
		getHeaderMessage().setText(R.string.str_settings_masterKey_change_three);
		showMsg(viewport.getResString(R.string.str_settings_masterKey_change_three));
	}

	private void stepThree(byte[] key) {
		if(key.length < 4 || key.length > 10) {
			showError(viewport.getResString(R.string.str_key_size_invalid));
			return;
		}
		clearKey();
		if(Arrays.equals(key, firstKey)) {
			showMsg(viewport.getResString(R.string.str_settings_masterKey_change_succeed));
			viewport.ignoreTouchTemporary();
			FileUtil.saveKey(key);
			DataUtil.flushDB();
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					viewport.enterMainView(getToastView());
				}
			}, viewport.calcAnimDurToLong(1000));
		}else{
			currentStep = 2;
			getHeaderMessage().setText(R.string.str_settings_masterKey_change_two);
			showError(viewport.getResString(R.string.str_key_wrong));
		}
	}
}
