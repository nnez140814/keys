package me.tranglenull.keys.view;

import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import me.tranglenull.keys.common.FileUtil;
import me.tranglenull.keys.common.PermissionUtil;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;

public class RestoreView implements View.OnClickListener {
	private Viewport viewport;
	private View view;
	private View toolbar;
	private View content;
	private LinearLayout list;

	public RestoreView(Viewport viewport) {
		this.viewport = viewport;
		view = View.inflate(viewport, R.layout.view_restore, null);
		toolbar = view.findViewById(R.id.settings_restore_header);
		content = view.findViewById(R.id.settings_restore_content);
		list = (LinearLayout) view.findViewById(R.id.settings_restore_list);

		view.findViewById(R.id.settings_restore_back).setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.settings_restore_back:
				viewport.exitToSettingsView(v);
				break;
			case R.id.restore_item:
				final String fileName = ((TextView) (v.findViewById(R.id.restore_item_name))).getText().toString();
				viewport.enterRestoreVerifyView(v, fileName, new RestoreVerifyView.OnKeySubmitListener() {
					@Override
					public void onKeySubmit(final RestoreVerifyView verifyView, byte[] key) {
						if(FileUtil.restore(fileName, key)) {
							verifyView.showMsg(viewport.getResString(R.string.str_settings_data_restore_succeed));
							verifyView.clearKey();
							viewport.ignoreTouchTemporary();
							FileUtil.saveKey(key);
							Handler handler = new Handler();
							handler.postDelayed(new Runnable() {
								@Override
								public void run() {
									viewport.enterMainView(verifyView.getToastView());
								}
							}, viewport.calcAnimDurToLong(1000));
						}else{
							verifyView.showError(viewport.getResString(R.string.str_settings_data_restore_failed));
						}
					}
				});
				break;
		}
	}

	public void onShow() {
		list.removeAllViews();
		if(PermissionUtil.isStorageAccessible(viewport)) {
			String[] backups = FileUtil.getAllBackupNames();
			for(int i = 0; i < backups.length; i++) {
				View item = View.inflate(viewport, R.layout.component_restore_list_item, null);
				((TextView) (item.findViewById(R.id.restore_item_name))).setText(backups[i]);
				item.setOnClickListener(this);
				list.addView(item);
			}
		}else{
			viewport.makeDialog(viewport.getResString(R.string.str_permission),
					viewport.getResString(R.string.str_permission_storage),
					viewport.getResString(R.string.str_permission_decline),
					viewport.getResString(R.string.str_permission_set),
					new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							viewport.exitToSettingsView(v);
						}
					}, new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							viewport.ignoreOnPauseTemporary();
							if(!PermissionUtil.requestStorageAccessPermission(viewport)){
								viewport.makeSnackBar(viewport.getResString(R.string.str_permission_storage_manual));
								viewport.exitToSettingsView(view);
							}
						}
					});
		}
	}

	/* getters */
	public View getView() {
		return view;
	}

	public View getToolbar() {
		return toolbar;
	}

	public View getContent() {
		return content;
	}
}
