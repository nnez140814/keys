package me.tranglenull.keys.view;

import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;

public class RestoreVerifyView extends KeyView {
	OnKeySubmitListener listener;

	public RestoreVerifyView(Viewport viewport) {
		super(viewport);
		getHeaderTitle().setText(R.string.str_settings_data_restore);
	}

	public void onShow(String backupFileName, OnKeySubmitListener listener) {
		super.onShow();
		this.listener = listener;
		getHeaderMessage().setText(String.format(viewport.getResString(R.string.str_settings_data_restore_confirm), backupFileName));
	}

	@Override
	protected void onKeySubmit() {
		listener.onKeySubmit(this, pwdStack.toByteArray());
	}

	public interface OnKeySubmitListener {
		void onKeySubmit(RestoreVerifyView verifyView, byte[] key);
	}

}
