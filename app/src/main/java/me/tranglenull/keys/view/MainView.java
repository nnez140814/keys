package me.tranglenull.keys.view;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.macaroni.androidLib.ScrollView;
import me.tranglenull.keys.common.DataUtil;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;
import org.json.JSONArray;
import org.json.JSONObject;

public class MainView implements View.OnClickListener, View.OnTouchListener {
	private View view;
	private LinearLayout header;
	private ScrollView container;
	private LinearLayout drawerContainer;
	private LinearLayout drawerHeader;
	private LinearLayout recordContainer;

	private Viewport viewport;

	private int touchX, touchY;
	private int touchViewID;

	private final String KEY_MASK = "************";

	public MainView(Viewport viewport) {
		this.viewport = viewport;

		view = View.inflate(viewport, R.layout.view_main, null);
		header = (LinearLayout) view.findViewById(R.id.main_header);
		container = (ScrollView) view.findViewById(R.id.main_container);
		drawerContainer = (LinearLayout) view.findViewById(R.id.main_drawer_container);
		drawerHeader = (LinearLayout) view.findViewById(R.id.main_drawer_header);
		recordContainer = (LinearLayout) view.findViewById(R.id.main_record_container);
		view.findViewById(R.id.main_new_record).setOnClickListener(this);

		view.findViewById(R.id.main_drawer_setting).setOnClickListener(this);
		view.findViewById(R.id.main_drawer_setting).setOnTouchListener(this);
	}

	public void onShow() {
		recordContainer.removeAllViews();
		getView().requestFocus();
		JSONArray db = DataUtil.getDatabase();
		try {
			for(int i = 0; i < db.length(); i++) {
				JSONObject object = db.getJSONObject(i);
				View view = View.inflate(viewport, R.layout.component_record_card, null);
				view.setTag(i);
				((TextView) (view.findViewById(R.id.card_description))).setText(object.getString("Description"));
				((TextView) (view.findViewById(R.id.card_account))).setText(object.getString("Account"));
				((TextView) (view.findViewById(R.id.card_password))).setText(KEY_MASK);
				view.setOnClickListener(this);
				recordContainer.addView(view);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void onResume() {
		recordContainer.removeAllViews();
	}

	Handler handler = new Handler();
	@Override
	public void onClick(final View v) {
		switch(v.getId()) {
			case R.id.main_new_record:
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						viewport.enterNewRecordView(v);
					}
				}, viewport.calcAnimDurToLong(200));
				break;
			case R.id.main_drawer_setting:
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						if(touchViewID == R.id.main_drawer_setting) {
							viewport.enterSettingsView(touchX, touchY);
						}else{
							viewport.enterSettingsView(v);
						}
					}
				}, viewport.calcAnimDurToLong(200));
				break;
			default:
				// is a component
				if(v.getTag() != null) {
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							if(touchViewID == R.id.main_drawer_setting) {
								viewport.enterKeyDetailView(touchX, touchY, (int) v.getTag());
							}else{
								viewport.enterKeyDetailView(v, (int) v.getTag());
							}
						}
					}, viewport.calcAnimDurToLong(200));
				}
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if(event.getAction() != MotionEvent.ACTION_UP)
			return false;
		touchViewID = v.getId();
		touchX = (int) event.getRawX();
		touchY = (int) event.getRawY();
		return false;
	}

	/* getter */
	public View getView() {
		return view;
	}

	public LinearLayout getHeader() {
		return header;
	}

	public LinearLayout getDrawerHeader() {
		return drawerHeader;
	}

	public ScrollView getContainer() {
		return container;
	}

	public LinearLayout getDrawerContainer() {
		return drawerContainer;
	}

}
