package me.tranglenull.keys.view;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import me.tranglenull.keys.common.DataUtil;
import me.tranglenull.keys.KeyCopyProvider;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;
import org.json.JSONException;
import org.json.JSONObject;

public class KeyDetailView implements View.OnClickListener, View.OnLongClickListener {
	private Viewport viewport;
	private View view;

	private View container;
	private View header;
	private TextView description;
	private TextView account;
	private TextView key;

	public KeyDetailView(Viewport viewport) {
		this.viewport = viewport;
		this.view = View.inflate(viewport, R.layout.view_key_detail, null);

		container = view.findViewById(R.id.key_detail_container);
		header = view.findViewById(R.id.key_detail_header);
		description = (TextView) view.findViewById(R.id.key_detail_description_tv);
		account = (TextView) view.findViewById(R.id.key_detail_account_tv);
		key = (TextView) view.findViewById(R.id.key_detail_key_tv);

		view.findViewById(R.id.key_detail_back).setOnClickListener(this);

		view.findViewById(R.id.key_detail_key).setOnLongClickListener(this);
	}

	public View getView() {
		return view;
	}

	public void onShow(int recordPosition) {
		try {
			JSONObject object = DataUtil.getDatabase().getJSONObject(recordPosition);
			description.setText(object.getString("Description"));
			account.setText(object.getString("Account"));
			key.setText(object.getString("Password"));
		}catch(JSONException e) {
			e.printStackTrace();
			viewport.makeSnackBar(viewport.getResString(R.string.str_record_detail_init_failed));
		}
	}

	public void onPause() {
		description.setText(null);
		account.setText(null);
		key.setText(null);
	}

	public View getHeader() {
		return header;
	}

	public View getContainer() {
		return container;
	}

	Handler handler = new Handler();
	@Override
	public void onClick(final View v) {
		switch(v.getId()) {
			case R.id.key_detail_back:
				viewport.ignoreTouchTemporary();
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						viewport.enterMainView(v);
					}
				}, viewport.calcAnimDurToLong(150));
				break;
		}
	}

	@Override
	public boolean onLongClick(View v) {
		switch(v.getId()) {
			case R.id.key_detail_key:
				ClipboardManager clipboardManager = (ClipboardManager) viewport.getSystemService(Context.CLIPBOARD_SERVICE);
				KeyCopyProvider.setKey(key.getText().toString());
				Uri copyUri = Uri.parse(KeyCopyProvider.KEYS);
				ClipData data = ClipData.newUri(viewport.getContentResolver(), "Password", copyUri);
				clipboardManager.setPrimaryClip(data);
				viewport.makeSnackBar("密码已复制");
				break;
			default:
				return false;
		}
		return true;
	}
}

