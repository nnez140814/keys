package me.tranglenull.keys.view;

import android.os.Handler;
import com.macaroni.androidLib.Tools;
import me.tranglenull.keys.common.DataUtil;
import me.tranglenull.keys.common.FileUtil;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;

public class KeySetupView extends KeyView {

	public KeySetupView(Viewport viewport) {
		super(viewport);
		getHeaderTitle().setText(R.string.str_welcome);
		getHeaderMessage().setText(R.string.str_key_setup);
	}

	private byte[] lastKey;
	@Override
	protected void onKeySubmit() {
		System.out.println("KeySetupView.onKeySubmit()");
		// Check Key size
		if(pwdStack.size() < 4 || pwdStack.size() > 10) {
			showError(viewport.getResString(R.string.str_key_size_invalid));
			return;
		}
		// Input again to verify
		if(lastKey == null) {
			showMsg(viewport.getResString(R.string.str_key_setup_check));
			lastKey = pwdStack.toByteArray();
			pwdStack.clear();
			updatePwdDisplay();
			return;
		}
		// Check if the second input is equal
		if(!Tools.isArrayEqual(lastKey, pwdStack.toByteArray())) {
			showError(viewport.getResString(R.string.str_key_setup_check_failed));
			lastKey = null;
			pwdStack.clear();
			updatePwdDisplay();
			return;
		}
		// Save Key to file
		System.out.println("Saving key!");
		String result = FileUtil.saveKey(pwdStack.toByteArray());
		if(result != null) {
			showError(result);
			return;
		}
		System.out.println("Key saved!");
		// Setup Database
		if(!DataUtil.setup()) {
			showError(viewport.getResString(R.string.str_db_setup_failed));
			return;
		}
		// Init Database
		if(!DataUtil.init()) {
			showError(viewport.getResString(R.string.str_db_init_failed));
			return;
		}
		// Enter MainView
		showMsg(viewport.getResString(R.string.str_key_right));
		viewport.ignoreTouchTemporary();
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				viewport.enterMainView(view.findViewById(R.id.key_toast));
			}
		}, viewport.calcAnimDurToLong(1000));
	}
}
