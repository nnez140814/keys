package me.tranglenull.keys.view;

import android.os.Handler;
import me.tranglenull.keys.common.DataUtil;
import me.tranglenull.keys.common.FileUtil;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;

public class KeyVerifyView extends KeyView {
	public KeyVerifyView(Viewport viewport) {
		super(viewport);
		getHeaderTitle().setText(R.string.str_welcome);
		getHeaderMessage().setText(R.string.str_key_verify);
	}

	protected void onKeySubmit() {
		System.out.println("KeyView.onKeySubmit()");
		// Check Key size
		if(pwdStack.size() < 4 || pwdStack.size() > 10) {
			showError(viewport.getResString(R.string.str_key_size_invalid));
			return;
		}
		// Verify Key
		if(!FileUtil.verifyKey(pwdStack.toByteArray())) {
			showError(viewport.getResString(R.string.str_key_wrong));
			return;
		}
		// Init Database
		if(!DataUtil.init()) {
			showError(viewport.getResString(R.string.str_db_init_failed));
			return;
		}
		// Enter MainView
		viewport.ignoreTouchTemporary();
		pwdStack.clear();
		updatePwdDisplay();
		viewport.enterSavedView(view.findViewById(R.id.key_toast));
	}

	@Override
	protected void onKeyUpdate() {
		if(pwdStack.size() < 4 || pwdStack.size() > 10) {
			return;
		}
		if(!FileUtil.verifyKey(pwdStack.toByteArray())) {
			return;
		}
		if(!DataUtil.init()) {
			showError(viewport.getResString(R.string.str_db_init_failed));
			return;
		}
		viewport.ignoreTouchTemporary();
		pwdStack.clear();
		updatePwdDisplay();
		viewport.enterSavedView(view.findViewById(R.id.key_toast));
	}
}
