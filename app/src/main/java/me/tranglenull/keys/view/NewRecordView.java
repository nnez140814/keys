package me.tranglenull.keys.view;

import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import com.rengwuxian.materialedittext.MaterialEditText;
import me.tranglenull.keys.common.DataUtil;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;

public class NewRecordView implements View.OnClickListener {
	private Viewport viewport;

	private View view;
	private LinearLayout header;
	private MaterialEditText description_et;
	private MaterialEditText account_et;
	private MaterialEditText password_et;

	public NewRecordView(Viewport viewport) {
		this.viewport = viewport;

		view = View.inflate(viewport, R.layout.view_new_record, null);
		header = (LinearLayout) view.findViewById(R.id.new_record_header);
		description_et = (MaterialEditText) view.findViewById(R.id.new_record_description);
		account_et = (MaterialEditText) view.findViewById(R.id.new_record_account);
		password_et = (MaterialEditText) view.findViewById(R.id.new_record_password);

		view.findViewById(R.id.new_record_back).setOnClickListener(this);
		view.findViewById(R.id.new_record_done).setOnClickListener(this);
	}

	public View getView() {
		return view;
	}

	public LinearLayout getHeader() {
		return header;
	}

	public void onShow() {
		getView().requestFocus();
		description_et.setText(null);
		account_et.setText(null);
		password_et.setText(null);
	}

	Handler handler = new Handler();
	@Override
	public void onClick(final View v) {
		switch(v.getId()) {
			case R.id.new_record_back:
				viewport.ignoreTouchTemporary();
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						viewport.exitToMainView(v);
					}
				}, viewport.calcAnimDurToLong(150));
				viewport.hideKeyPad(description_et.getWindowToken());
				break;
			case R.id.new_record_done:
				if(!DataUtil.addRecord(description_et.getText().toString(), account_et.getText().toString(), password_et.getText().toString())) {
					viewport.makeSnackBar(viewport.getResString(R.string.str_record_description_exists));
					break;
				}
				viewport.makeSnackBar(viewport.getResString(R.string.str_record_new_succeed));
				viewport.ignoreTouchTemporary();
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						viewport.exitToMainView(v);
					}
				}, viewport.calcAnimDurToLong(150));
				viewport.hideKeyPad(description_et.getWindowToken());
				break;
		}
	}
}
