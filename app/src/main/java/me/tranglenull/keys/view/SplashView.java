package me.tranglenull.keys.view;

import android.content.Context;
import android.view.View;
import me.tranglenull.keys.R;

public class SplashView {
	private View view;

	public SplashView(Context context) {
		view = View.inflate(context, R.layout.view_splash, null);
	}

	public View getView() {
		return view;
	}
}
