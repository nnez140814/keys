package me.tranglenull.keys.view;

import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.macaroni.androidLib.ResizableIntStack;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;

public class KeyView implements View.OnClickListener, View.OnLongClickListener {
	protected Viewport viewport;

	protected View view;
	private LinearLayout header;
	private TextView headerTitle;
	private TextView headerMessage;
	private LinearLayout keyPad;
	private TextView pwdDisplay;
	protected ResizableIntStack pwdStack;
	protected TextView toastView;

	public KeyView(Viewport viewport) {
		this.viewport = viewport;

		view = View.inflate(viewport, R.layout.view_key, null);
		header = (LinearLayout) view.findViewById(R.id.key_header);
		headerTitle = (TextView) view.findViewById(R.id.key_header_headline);
		headerMessage = (TextView) view.findViewById(R.id.key_header_message);
		keyPad = (LinearLayout) view.findViewById(R.id.key_pad);
		pwdDisplay = (TextView) view.findViewById(R.id.key_display);
		toastView = (TextView) view.findViewById(R.id.key_toast);

		// Set Key Listener
		view.findViewById(R.id.key_1).setOnClickListener(this);
		view.findViewById(R.id.key_2).setOnClickListener(this);
		view.findViewById(R.id.key_3).setOnClickListener(this);
		view.findViewById(R.id.key_4).setOnClickListener(this);
		view.findViewById(R.id.key_5).setOnClickListener(this);
		view.findViewById(R.id.key_6).setOnClickListener(this);
		view.findViewById(R.id.key_7).setOnClickListener(this);
		view.findViewById(R.id.key_8).setOnClickListener(this);
		view.findViewById(R.id.key_9).setOnClickListener(this);
		view.findViewById(R.id.key_0).setOnClickListener(this);

		view.findViewById(R.id.key_del).setOnClickListener(this);
		view.findViewById(R.id.key_del).setOnLongClickListener(this);

		view.findViewById(R.id.key_OK).setOnClickListener(this);

		pwdStack = new ResizableIntStack();
	}

	public View getView() {
		return view;
	}

	public LinearLayout getHeader() {
		return header;
	}

	public TextView getHeaderTitle() {
		return headerTitle;
	}

	public TextView getHeaderMessage() {
		return headerMessage;
	}

	public LinearLayout getKeyPad() {
		return keyPad;
	}

	public TextView getToastView() {
		return toastView;
	}

	protected void updatePwdDisplay() {
		int length = pwdStack.size();
		char[] pwdMask = new char[length];
		pwdDisplay.setText(pwdMask, 0, length);
	}

	public void onShow() {
		toastView.setTranslationY(toastView.getHeight());
	}


	Handler toastHandler = new Handler();
	Runnable hideToastRunnable = new Runnable() {
		@Override
		public void run() {
			toastView.animate().translationY(toastView.getHeight()).setDuration(300).setInterpolator(new AccelerateInterpolator()).start();
		}
	};

	protected void showMsg(String msg) {
		toastHandler.removeCallbacks(hideToastRunnable);
		toastView.setBackgroundResource(R.color.accentColor);
		toastView.setText(msg);
		toastView.setTranslationY(toastView.getHeight());
		toastView.animate().translationY(0).setDuration(500).setInterpolator(new DecelerateInterpolator()).start();
		toastHandler.postDelayed(hideToastRunnable, viewport.calcAnimDurToLong(2500));
	}

	protected void showError(String error) {
		toastHandler.removeCallbacks(hideToastRunnable);
		toastView.setBackgroundResource(R.color.error);
		toastView.setText(error);
		toastView.setTranslationY(toastView.getHeight());
		toastView.animate().translationY(0).setDuration(500).setInterpolator(new DecelerateInterpolator()).start();
		toastHandler.postDelayed(hideToastRunnable, viewport.calcAnimDurToLong(2500));
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.key_1:
				pwdStack.push(1);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_2:
				pwdStack.push(2);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_3:
				pwdStack.push(3);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_4:
				pwdStack.push(4);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_5:
				pwdStack.push(5);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_6:
				pwdStack.push(6);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_7:
				pwdStack.push(7);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_8:
				pwdStack.push(8);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_9:
				pwdStack.push(9);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_0:
				pwdStack.push(0);
				updatePwdDisplay();
				onKeyUpdate();
				break;
			case R.id.key_del:
				if(pwdStack.size() > 0) {
					pwdStack.pop();
					updatePwdDisplay();
				}
				break;
			case R.id.key_OK:
				onKeySubmit();
				break;
		}
	}

	@Override
	public boolean onLongClick(View v) {
		switch(v.getId()) {
			case R.id.key_del:
				pwdStack.clear();
				updatePwdDisplay();
				return true;
		}
		return false;
	}

	public void clearKey() {
		pwdStack.clear();
		updatePwdDisplay();
	}

	protected void onKeySubmit() {}

	protected void onKeyUpdate() {}

}
