package me.tranglenull.keys.view;

import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import carbon.widget.Button;
import com.macaroni.androidLib.Queue;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;

public class OverlayView implements View.OnClickListener {
	private Viewport viewport;
	private View view;
	private LinearLayout snackBar;
	private TextView snackBarText;
	private Queue<String> snackBarMessages = new Queue<>();

	private LinearLayout dialog;
	private TextView dialogTitle;
	private LinearLayout dialogContentContainer;
	private Button dialogNegativeBtn;
	private Button dialogPositiveBtn;
	private View.OnClickListener dialogNegativeListener;
	private View.OnClickListener dialogPositiveListener;
	private boolean dialogClickOutSideToHide = true;

	public OverlayView(Viewport viewport) {
		this.viewport = viewport;

		view = View.inflate(viewport, R.layout.view_overlay, null);
		snackBar = (LinearLayout) view.findViewById(R.id.overlay_snack);
		snackBarText = (TextView) view.findViewById(R.id.overlay_snack_text);

		snackBar.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			@Override
			public boolean onPreDraw() {
				snackBar.setTranslationY(snackBar.getHeight());
				snackBar.getViewTreeObserver().removeOnPreDrawListener(this);
				return true;
			}
		});

		dialog = (LinearLayout) view.findViewById(R.id.overlay_dialog);
		dialogTitle = (TextView) view.findViewById(R.id.overlay_dialog_title);
		dialogContentContainer = (LinearLayout) view.findViewById(R.id.overlay_dialog_content);
		dialogNegativeBtn = (Button) view.findViewById(R.id.overlay_dialog_negative);
		dialogPositiveBtn = (Button) view.findViewById(R.id.overlay_dialog_positive);
		dialog.setOnClickListener(this);
		dialogNegativeBtn.setOnClickListener(this);
		dialogPositiveBtn.setOnClickListener(this);
	}

	public View getView() {
		return view;
	}

	public void makeSnackBar(String msg) {
		snackBarMessages.enqueue(msg);
		if(snackBarMessages.size() == 1)
			showSnackBar();
	}

	Handler snackBarHandler = new Handler();
	private void showSnackBar() {
		snackBarText.setText(snackBarMessages.getNext());
		snackBar.animate().translationY(0).setDuration(300).setInterpolator(new DecelerateInterpolator()).start();
		snackBarHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				hideSnackBar();
			}
		}, viewport.calcAnimDurToLong(2500));
	}

	private void hideSnackBar() {
		snackBar.animate().translationY(snackBar.getHeight()).setDuration(300).setInterpolator(new AccelerateInterpolator()).start();
		snackBarHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				snackBarMessages.dequeue();
				if(!snackBarMessages.isEmpty()) {
					showSnackBar();
				}
			}
		}, viewport.calcAnimDurToLong(300));
	}

	public void showDialog(String title, String content, String negative, String positive, View.OnClickListener negativeListener, View.OnClickListener positiveListener) {
		dialogTitle.setText(title);
		TextView contentView = new TextView(viewport);
		contentView.setText(content);
		dialogContentContainer.removeAllViews();
		dialogContentContainer.addView(contentView);
		dialogNegativeBtn.setText(negative);
		dialogPositiveBtn.setText(positive);
		dialogNegativeListener = negativeListener;
		dialogPositiveListener = positiveListener;
		dialog.setVisibility(View.VISIBLE);
	}

	public void hideDialog() {
		dialog.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.overlay_dialog:
				if(dialogClickOutSideToHide) {
					hideDialog();
				}
				break;
			case R.id.overlay_dialog_negative:
				hideDialog();
				if(dialogNegativeListener != null) {
					dialogNegativeListener.onClick(v);
				}
				break;
			case R.id.overlay_dialog_positive:
				hideDialog();
				if(dialogPositiveListener != null) {
					dialogPositiveListener.onClick(v);
				}
				break;
		}
	}

	public LinearLayout getSnackBar() {
		return snackBar;
	}

	public LinearLayout getDialog() {
		return dialog;
	}
}
