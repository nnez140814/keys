package me.tranglenull.keys.view;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import me.tranglenull.keys.common.FileUtil;
import me.tranglenull.keys.common.FingerprintUtil;
import me.tranglenull.keys.common.PermissionUtil;
import me.tranglenull.keys.R;
import me.tranglenull.keys.Viewport;

public class SettingsView implements View.OnClickListener, View.OnTouchListener {
    private Viewport viewport;
    private View view;
    private View toolbar;
    private View content;

    private int touchX, touchY;
    private int touchViewID;

    public SettingsView(Viewport viewport) {
        this.viewport = viewport;
        view = View.inflate(viewport, R.layout.view_settings, null);
        toolbar = view.findViewById(R.id.settings_header);
        content = view.findViewById(R.id.settings_content);
        view.findViewById(R.id.settings_back).setOnClickListener(this);
        view.findViewById(R.id.settings_data_backup).setOnClickListener(this);
        view.findViewById(R.id.settings_data_restore).setOnClickListener(this);
        view.findViewById(R.id.settings_data_restore).setOnTouchListener(this);
        view.findViewById(R.id.settings_masterKey_change).setOnClickListener(this);
        view.findViewById(R.id.settings_masterKey_change).setOnTouchListener(this);
        if(FingerprintUtil.isFingerprintSupported(viewport)) {
            view.findViewById(R.id.settings_fingerprint).setOnClickListener(this);
        }else{
            view.findViewById(R.id.settings_fingerprint).setVisibility(View.GONE);
        }
    }

    Handler handler = new Handler();

    @Override
    public void onClick(final View v) {
        switch(v.getId()) {
            case R.id.settings_back:
                viewport.ignoreTouchTemporary();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        viewport.exitToMainView(v);
                    }
                }, viewport.calcAnimDurToLong(150));
                break;
            case R.id.settings_data_backup:
                if(!PermissionUtil.isStorageAccessible(viewport)) {
                    viewport.makeDialog(viewport.getResString(R.string.str_permission),
                            viewport.getResString(R.string.str_permission_storage),
                            viewport.getResString(R.string.str_permission_decline),
                            viewport.getResString(R.string.str_permission_set),
                            null,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    viewport.ignoreOnPauseTemporary();
                                    if(!PermissionUtil.requestStorageAccessPermission(viewport)) {
                                        viewport.makeSnackBar(viewport.getResString(R.string.str_permission_storage_manual));
                                    }
                                }
                            });
                    return;
                }
                viewport.makeDialog(viewport.getResString(R.string.str_settings_data_backup),
                        viewport.getResString(R.string.str_settings_data_backup_intro),
                        viewport.getResString(R.string.str_settings_data_backup_cancel),
                        viewport.getResString(R.string.str_settings_data_backup),
                        null,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(FileUtil.backup()) {
                                    viewport.makeSnackBar(viewport.getResString(R.string.str_settings_data_backup_succeed));
                                }else{
                                    viewport.makeSnackBar(viewport.getResString(R.string.str_settings_data_backup_failed));
                                }
                            }
                        });
                break;
            case R.id.settings_data_restore:
                viewport.ignoreTouchTemporary();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(touchViewID == R.id.settings_data_restore) {
                            viewport.enterRestoreView(touchX, touchY);
                        }else{
                            viewport.enterRestoreView(v);
                        }
                    }
                }, viewport.calcAnimDurToLong(200));
                break;
            case R.id.settings_masterKey_change:
                viewport.ignoreTouchTemporary();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(touchViewID == R.id.settings_masterKey_change) {
                            viewport.enterChangeKeyView(touchX, touchY);
                        }else{
                            viewport.enterChangeKeyView(v);
                        }
                    }
                }, viewport.calcAnimDurToLong(200));
                break;
            case R.id.settings_fingerprint:
                // TODO
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() != MotionEvent.ACTION_UP)
            return false;
        touchViewID = v.getId();
        touchX = (int) event.getRawX();
        touchY = (int) event.getRawY();
        return false;
    }

    public void onShow() {
        getView().requestFocus();
        getView().post(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    /* getters */
    public View getView() {
        return view;
    }

    public View getToolbar() {
        return toolbar;
    }

    public View getContent() {
        return content;
    }
}
