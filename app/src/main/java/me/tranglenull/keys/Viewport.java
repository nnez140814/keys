package me.tranglenull.keys;

import android.Manifest;
import android.app.ActivityManager;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.view.WindowInsets;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import com.macaroni.androidLib.TransitionLayout;
import com.macaroni.androidLib.TranslucentActivity;
import me.tranglenull.keys.common.FileUtil;
import me.tranglenull.keys.view.*;

public class Viewport extends TranslucentActivity {
	private final static int splashViewIndex = 0;
	private final static int keyVerifyViewIndex = 1;
	private final static int keySetupViewIndex = 2;
	private final static int mainViewIndex = 3;
	private final static int newRecordViewIndex = 4;
	private final static int overlayViewIndex = 5;
	private final static int settingsViewIndex = 6;
	private final static int restoreViewIndex = 7;
	private final static int restoreVerifyViewIndex = 8;
	private final static int changeKeyViewIndex = 9;
	private final static int keyDetailViewIndex = 10;

	private int savedViewIndex = splashViewIndex;
	private boolean shouldOnPauseBeIgnoredTemporary = false;

	private TransitionLayout rootView;
	private SplashView splashView;
	private KeyVerifyView keyVerifyView;
	private KeySetupView keySetupView;
	private MainView mainView;
	private NewRecordView newRecordView;
	private OverlayView overlayView;
	private SettingsView settingsView;
	private RestoreView restoreView;
	private RestoreVerifyView restoreVerifyView;
	private KeyChangeView keyChangeView;
	private KeyDetailView keyDetailView;

	private static float animationScale = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
			setTaskDescription(new ActivityManager.TaskDescription(getString(R.string.app_name), BitmapFactory.decodeResource(getResources(), R.mipmap.keys_no_bg), getResColor(R.color.lightBlue600)));
		}

		/* Load views */
		rootView = new TransitionLayout(this);
		setContentView(rootView);

		FileUtil.init(getBaseContext());

		splashView = new SplashView(this);
		rootView.addView(splashView.getView(), splashViewIndex);
//		rootView.setCurrentChild(splashViewIndex);

		keyVerifyView = new KeyVerifyView(this);
		rootView.addView(keyVerifyView.getView(), keyVerifyViewIndex);
		rootView.setTopmostViewIndex(keyVerifyViewIndex);

		keySetupView = new KeySetupView(this);
		rootView.addView(keySetupView.getView(), keySetupViewIndex);

		mainView = new MainView(this);
		rootView.addView(mainView.getView(), mainViewIndex);

		newRecordView = new NewRecordView(this);
		rootView.addView(newRecordView.getView(), newRecordViewIndex);

		overlayView = new OverlayView(this);
		rootView.addView(overlayView.getView(), overlayViewIndex);
		rootView.setOverlayViewIndex(overlayViewIndex);

		settingsView = new SettingsView(this);
		rootView.addView(settingsView.getView(), settingsViewIndex);

		restoreView = new RestoreView(this);
		rootView.addView(restoreView.getView(), restoreViewIndex);

		restoreVerifyView = new RestoreVerifyView(this);
		rootView.addView(restoreVerifyView.getView(), restoreVerifyViewIndex);

		keyChangeView = new KeyChangeView(this);
		rootView.addView(keyChangeView.getView(), changeKeyViewIndex);

		keyDetailView = new KeyDetailView(this);
		rootView.addView(keyDetailView.getView(), keyDetailViewIndex);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
			rootView.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
				@Override
				public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
					System.out.println(insets);
					if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
						setHeadersPadding(insets.getSystemWindowInsetTop(), keyVerifyView.getHeader(), keySetupView.getHeader(), mainView.getHeader(), mainView.getDrawerHeader(), newRecordView.getHeader(), settingsView.getToolbar(), overlayView.getDialog(), restoreView.getToolbar(), restoreVerifyView.getHeader(), keyChangeView.getHeader(), keyDetailView.getHeader());
						setFootersPadding(insets.getSystemWindowInsetBottom(), keyVerifyView.getKeyPad(), keySetupView.getKeyPad(), mainView.getContainer(), mainView.getDrawerContainer(), overlayView.getSnackBar(), settingsView.getContent(), overlayView.getDialog(), restoreView.getContent(), restoreVerifyView.getKeyPad(), keyChangeView.getKeyPad(), keyDetailView.getContainer());
					}
					return insets;
				}
			});
		}

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				rootView.setHotspot(findViewById(R.id.splash_icon));
				keySetupView.onShow();
				keyVerifyView.onShow();

				if(FileUtil.isFirstLaunch()) {
					System.out.println("Launch KeySetupView");
					rootView.startTransition(keySetupViewIndex, TransitionLayout.TransitionType.Radial, 800, false, new AccelerateDecelerateInterpolator());
				}else{
					rootView.startTransition(keyVerifyViewIndex, TransitionLayout.TransitionType.Radial, 800, false, new AccelerateDecelerateInterpolator());
				}
			}
		}, 1000);

		/* Init animationScale */
		try {
			animationScale = (float) Class.forName("android.animation.ValueAnimator").getMethod("getDurationScale").invoke(null);
		}catch(Exception e) {
			e.printStackTrace();
			animationScale = 1;
		}

		/* Menu Key */
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(shouldOnPauseBeIgnoredTemporary) {
			return;
		}
		FileUtil.clearKey();
		keyDetailView.onPause();
		if(rootView.getCurrentChild() == splashViewIndex) {
			return;
		}
		rootView.setCurrentChildWithoutPostInvalidate(FileUtil.isFirstLaunch() ? keySetupViewIndex : keyVerifyViewIndex);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(shouldOnPauseBeIgnoredTemporary) {
			shouldOnPauseBeIgnoredTemporary = false;
			return;
		}
		mainView.onResume();
		rootView.invalidate();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		for(int i = 0; i < permissions.length; i++) {
			switch(permissions[i]) {
				case Manifest.permission.WRITE_EXTERNAL_STORAGE:
					if(savedViewIndex == restoreViewIndex) {
						if(grantResults[i] == PackageManager.PERMISSION_GRANTED) {
							restoreView.onShow();
						}else{
							exitToSettingsView(rootView);
						}
					}
					break;
			}
		}
	}

	private void setSavedViewIndex(int index) {
		savedViewIndex = index;
	}

	public void enterMainView(View hotspot) {
		setSavedViewIndex(mainViewIndex);
		mainView.onShow();
		rootView.setHotspot(hotspot);
		rootView.startTransition(mainViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void exitToMainView(View hotspot) {
		setSavedViewIndex(mainViewIndex);
		mainView.onShow();
		rootView.setHotspot(hotspot);
		rootView.startTransition(mainViewIndex, TransitionLayout.TransitionType.Radial, 500, false);
	}

	public void enterNewRecordView(View hotspot) {
		setSavedViewIndex(newRecordViewIndex);
		newRecordView.onShow();
		rootView.setHotspot(hotspot);
		rootView.startTransition(newRecordViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void enterNewRecordView(int touchX, int touchY) {
		setSavedViewIndex(newRecordViewIndex);
		newRecordView.onShow();
		rootView.setHotspot(touchX, touchY);
		rootView.startTransition(newRecordViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void enterSettingsView(View hotspot) {
		setSavedViewIndex(settingsViewIndex);
		settingsView.onShow();
		rootView.setHotspot(hotspot);
		rootView.startTransition(settingsViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void enterSettingsView(int touchX, int touchY) {
		setSavedViewIndex(settingsViewIndex);
		settingsView.onShow();
		rootView.setHotspot(touchX, touchY);
		rootView.startTransition(settingsViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void exitToSettingsView(View hotspot) {
		setSavedViewIndex(settingsViewIndex);
		settingsView.onShow();
		rootView.setHotspot(hotspot);
		rootView.startTransition(savedViewIndex, TransitionLayout.TransitionType.Radial, 500, false);
	}

	public void enterRestoreView(View hotspot) {
		setSavedViewIndex(restoreViewIndex);
		restoreView.onShow();
		rootView.setHotspot(hotspot);
		rootView.startTransition(restoreViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void enterRestoreView(int touchX, int touchY) {
		setSavedViewIndex(restoreViewIndex);
		restoreView.onShow();
		rootView.setHotspot(touchX, touchY);
		rootView.startTransition(restoreViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void exitToRestoreView(View hotspot) {
		setSavedViewIndex(restoreViewIndex);
		restoreView.onShow();
		rootView.setHotspot(hotspot);
		rootView.startTransition(restoreViewIndex, TransitionLayout.TransitionType.Radial, 500, false);
	}

	public void enterRestoreVerifyView(View hotspot, String backupFileName, RestoreVerifyView.OnKeySubmitListener listener) {
		setSavedViewIndex(restoreViewIndex);
		restoreVerifyView.onShow(backupFileName, listener);
		rootView.setHotspot(hotspot);
		rootView.startTransition(restoreVerifyViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void enterChangeKeyView(View hotspot) {
		setSavedViewIndex(mainViewIndex);
		keyChangeView.onShow();
		rootView.setHotspot(hotspot);
		rootView.startTransition(changeKeyViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void enterChangeKeyView(int touchX, int touchY) {
		setSavedViewIndex(mainViewIndex);
		keyChangeView.onShow();
		rootView.setHotspot(touchX, touchY);
		rootView.startTransition(changeKeyViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void enterKeyDetailView(View hotspot, int recordPos) {
		setSavedViewIndex(keyDetailViewIndex);
		keyDetailView.onShow(recordPos);
		rootView.setHotspot(hotspot);
		rootView.startTransition(keyDetailViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void enterKeyDetailView(int touchX, int touchY, int recordPos) {
		setSavedViewIndex(keyDetailViewIndex);
		keyDetailView.onShow(recordPos);
		rootView.setHotspot(touchX, touchY);
		rootView.startTransition(keyDetailViewIndex, TransitionLayout.TransitionType.Radial, 500, true);
	}

	public void enterSavedView(View hotspot) {
		switch(savedViewIndex) {
			case newRecordViewIndex:
				enterNewRecordView(hotspot);
				break;
			case settingsViewIndex:
				enterSettingsView(hotspot);
				break;
			case restoreViewIndex:
				enterRestoreView(hotspot);
				break;
			case mainViewIndex:
			default:
				enterMainView(hotspot);
				break;
		}
	}

	public void makeSnackBar(String msg) {
		overlayView.makeSnackBar(msg);
	}

	public void makeDialog(String title, String content, String negative, String positive, View.OnClickListener negativeListener, View.OnClickListener positiveListener) {
		overlayView.showDialog(title, content, negative, positive, negativeListener, positiveListener);
	}

	public String getResString(int resid) {
		return getResources().getString(resid);
	}

	public int getResColor(int resid) {
		//noinspection deprecation
		return getResources().getColor(resid);
	}

	public int calcAnimDurToInt(int origin) {
		return (int) (origin * animationScale);
	}

	public long calcAnimDurToLong(int origin) {
		return (long) (origin * animationScale);
	}

	public void ignoreTouchTemporary() {
		rootView.ignoreTouchTemporary();
	}

	public void ignoreOnPauseTemporary() {
		shouldOnPauseBeIgnoredTemporary = true;
	}

	public void hideKeyPad(IBinder token) {
		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(token, 0);
	}

	@Override
	public void onBackPressed() {
		if(rootView.isAnimating()) {
			return;
		}
		switch(rootView.getCurrentChild()) {
			case newRecordViewIndex:
			case settingsViewIndex:
			case keyDetailViewIndex:
				exitToMainView(rootView);
				break;
			case restoreViewIndex:
				exitToSettingsView(rootView);
				break;
			case restoreVerifyViewIndex:
				exitToRestoreView(rootView);
				break;
			case changeKeyViewIndex:
				exitToSettingsView(rootView);
				break;
			default:
				super.onBackPressed();
		}
	}
}
