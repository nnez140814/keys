package me.tranglenull.keys;

import android.content.*;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class KeyCopyProvider extends ContentProvider {
	public static final String KEYS = "content://me.tranglenull.keys/copy/";

	private static String key;

	private static final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
	private static final int GET_KEY = 1;

	private Handler handler;

	@Override
	public boolean onCreate() {
		matcher.addURI(KEYS, "copy/*", GET_KEY);
		handler = new Handler();
		return true;
	}

	@Nullable
	@Override
	public AssetFileDescriptor openTypedAssetFile(Uri uri, String mimeTypeFilter, Bundle opts) throws FileNotFoundException {
		final int uid = Binder.getCallingUid();
//		final int pid = Binder.getCallingPid();

		return new AssetFileDescriptor(openPipeHelper(uri, mimeTypeFilter, opts, null, new PipeDataWriter<String>() {
			@Override
			public void writeDataToPipe(ParcelFileDescriptor output, Uri uri, String mimeType, Bundle opts, String args) {
				try {
					final FileOutputStream out = new FileOutputStream(output.getFileDescriptor());
					System.out.println(uid);
					if(key != null) {
						out.write(key.getBytes());
						handler.post(new Runnable() {
							@Override
							public void run() {
								App.clearClipboard();
								Toast.makeText(getContext(), String.format(App.getContext().getString(R.string.str_key_pasted), App.getAppNameByUID(uid)), Toast.LENGTH_LONG).show();
							}
						});
						key = null;
					}
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		}), 0, AssetFileDescriptor.UNKNOWN_LENGTH);
	}

	@Nullable
	@Override
	public String getType(Uri uri) {
		switch(matcher.match(uri)) {
			case GET_KEY:
				return ClipDescription.MIMETYPE_TEXT_PLAIN;
		}
		return ClipDescription.MIMETYPE_TEXT_PLAIN;
	}

	@Nullable
	@Override
	public String[] getStreamTypes(Uri uri, String mimeTypeFilter) {
		return new String[] {ClipDescription.MIMETYPE_TEXT_PLAIN};
	}

	@Nullable
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		return null;
	}

	@Nullable
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return 0;
	}

	public static void setKey(String key) {
		KeyCopyProvider.key = key;
	}
}
