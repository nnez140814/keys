package me.tranglenull.keys;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.widget.Toast;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class App extends Application implements Thread.UncaughtExceptionHandler {
	private static Context context;
	private static String unknownApp;
	private String crashText;

	@SuppressLint("SimpleDateFormat")
	private static DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

	private static final String APP_DIR = "/TrangleNull/Keys/";
	private static final String APP_LOG_DIR = APP_DIR + "Log/";
	private static final String APP_BACKUP_DIR = APP_DIR + "Backups/";

	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();
		crashText = getResources().getString(R.string.str_app_crashed);
		unknownApp = getString(R.string.str_app_unknown);

		Thread.setDefaultUncaughtExceptionHandler(this);
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				Toast.makeText(context, crashText, Toast.LENGTH_LONG).show();
				Looper.loop();
			}
		}.start();

		StringBuilder errorLogBuilder = new StringBuilder();
		errorLogBuilder.append("Keys Crash Log\n");

		buildDeviceLog(errorLogBuilder);
		buildCrashCause(ex, errorLogBuilder);

		writeTempCrashLog(errorLogBuilder);
		writeCrashLog(errorLogBuilder);

		System.out.println(errorLogBuilder.toString());

		try {
			Thread.sleep(3000);
		}catch(InterruptedException e) {
			e.printStackTrace();
		}

		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(1);
	}

	private void buildDeviceLog(StringBuilder errorLogBuilder) {
		errorLogBuilder.append("\n++App Info++");
		errorLogBuilder.append("\nVersion name: ");
		errorLogBuilder.append(BuildConfig.VERSION_NAME);
		errorLogBuilder.append("\nVersion code: ");
		errorLogBuilder.append(BuildConfig.VERSION_CODE);

		errorLogBuilder.append("\n\n++System Info++");
		errorLogBuilder.append("\nVersion: ");
		errorLogBuilder.append(Build.VERSION.RELEASE);
		errorLogBuilder.append("\nCodename: ");
		errorLogBuilder.append(Build.VERSION.CODENAME);
		errorLogBuilder.append("\nIncremental: ");
		errorLogBuilder.append(Build.VERSION.INCREMENTAL);
		errorLogBuilder.append("\nRelease: ");
		errorLogBuilder.append(Build.VERSION.RELEASE);
		errorLogBuilder.append("\nSDK: ");
		errorLogBuilder.append(Build.VERSION.SDK_INT);

		errorLogBuilder.append("\n\n++Device Info++");
		errorLogBuilder.append("\nBoard: ");
		errorLogBuilder.append(Build.BOARD);
		errorLogBuilder.append("\nBrand: ");
		errorLogBuilder.append(Build.BRAND);
		errorLogBuilder.append("\nDevice: ");
		errorLogBuilder.append(Build.DEVICE);
		errorLogBuilder.append("\nFinger print: ");
		errorLogBuilder.append(Build.FINGERPRINT);
		errorLogBuilder.append("\nHardware: ");
		errorLogBuilder.append(Build.HARDWARE);
		errorLogBuilder.append("\nID: ");
		errorLogBuilder.append(Build.ID);
		errorLogBuilder.append("\nManufacture: ");
		errorLogBuilder.append(Build.MANUFACTURER);
		errorLogBuilder.append("\nModel: ");
		errorLogBuilder.append(Build.MODEL);
		errorLogBuilder.append("\nProduct: ");
		errorLogBuilder.append(Build.PRODUCT);
		errorLogBuilder.append("\nSerial: ");
		errorLogBuilder.append(Build.SERIAL);
		errorLogBuilder.append("\nTags: ");
		errorLogBuilder.append(Build.TAGS);
		errorLogBuilder.append("\nType: ");
		errorLogBuilder.append(Build.TYPE);
		errorLogBuilder.append('\n');
		errorLogBuilder.append('\n');
	}

	private void buildCrashCause(Throwable ex, StringBuilder errorLogBuilder) {
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);

		ex.printStackTrace(printWriter);
		Throwable cause = ex.getCause();
		while(cause != null) {
			cause.printStackTrace(printWriter);
			cause = cause.getCause();
		}

		printWriter.close();
		errorLogBuilder.append(writer.toString());
	}

	private void writeTempCrashLog(StringBuilder errorLogBuilder) {
		File tmpLog = new File(context.getFilesDir(), "last_crash_log.log");
		try {
			FileOutputStream outputStream = new FileOutputStream(tmpLog);
			outputStream.write(errorLogBuilder.toString().getBytes());
			outputStream.close();
		}catch(IOException e) {
			System.err.println("Error while writing temp crash log.");
			e.printStackTrace();
		}
	}

	private void writeCrashLog(StringBuilder errorLogBuilder) {
		String time = formatter.format(System.currentTimeMillis());
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			String path = Environment.getExternalStorageDirectory().getPath() + APP_LOG_DIR;
			File dir = new File(path);
			if(!dir.exists()) {
				//noinspection ResultOfMethodCallIgnored
				dir.mkdirs();
			}
			File log = new File(dir, String.format("Crash-%s.log", time));
			try {
				FileOutputStream outputStream = new FileOutputStream(log);
				outputStream.write(errorLogBuilder.toString().getBytes());
				outputStream.close();
			}catch(IOException e) {
				System.err.println("Error while writing crash log.");
				e.printStackTrace();
			}

		}
	}

	public static String getAppNameByUID(int uid) {
		PackageManager packageManager = context.getPackageManager();

		for(ApplicationInfo applicationInfo : packageManager.getInstalledApplications(0)) {
			if(applicationInfo.uid == uid) {
				return applicationInfo.loadLabel(packageManager).toString();
			}
		}
		return unknownApp;
	}

	public static void clearClipboard() {
		ClipboardManager clipboardManager = (ClipboardManager) getContext().getSystemService(CLIPBOARD_SERVICE);
		clipboardManager.setText(null);
	}

	public static String formatTime() {
		return formatter.format(System.currentTimeMillis());
	}

	public static String getAppBackupDir() {
		return Environment.getExternalStorageDirectory().getPath() + APP_BACKUP_DIR;
	}

	public static Context getContext() {
		return context;
	}
}